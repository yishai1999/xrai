export interface AlgorithmResult {
  success: boolean;
  percentage: number;
}
