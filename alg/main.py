import json
import random
import sys

def main():
  success = True
  percentage = random.randint(0,100)
  res = json.dumps({
    'success': success,
    'percentage': percentage
  })
  sys.stdout.write(res + '\n')

if __name__ == '__main__':
  main()
