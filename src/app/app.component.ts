import { Component, isDevMode, NgZone } from '@angular/core';
import { ElectronService } from './core/electron.service';
import { join } from 'path';
import { exec } from 'child_process';
import { AlgorithmResult } from './lib/algorithm-result';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public percentage = 0;
  public imagePath = undefined;
  public imgURL = undefined;
  public notSupported = false;
  public badImage = false;

  constructor(private zone: NgZone) {
  }

  public onFileSelected(files: File[]) {
    if (files.length === 0)
      return;

    this.reset();
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.notSupported = true;
      return;
    }
    const reader = new FileReader();
    this.imagePath = files[0].path;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  public startAnalysis() {
    this.badImage = false;
    let algPath = '';
    if (isDevMode()) {
      algPath = join(__dirname, '..', '..', '..', '..', '..', '..', 'alg', 'main.py');
    } else {
      algPath = join(__dirname, '..', '..', '..', 'alg', 'main.py');
    }
    console.log(`executing: python ${algPath} ${this.imagePath} from ${__dirname}`);
    exec(`python ${algPath} ${this.imagePath}`, (err, stdout, stderr) => {
      if (err) {
        console.log(err);
      }
      else {
        console.log(`got ${stdout} from alg`);
        this.zone.run(() => {
          const res = JSON.parse(stdout) as AlgorithmResult;
          if (res.success) {
            this.percentage = res.percentage;
          } else {
            this.badImage = true;
            this.percentage = 0;
          }
        })
      }
    });
  }

  public reset() {
    this.percentage = 0;
    this.imagePath = undefined;
    this.imgURL = undefined;
    this.notSupported = false;
    this.badImage = false;
  }
}
