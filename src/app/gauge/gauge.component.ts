import { Component, OnInit, Input } from '@angular/core';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})
export class GaugeComponent implements OnInit {
  public _percentage = 0;
  private hand: am4charts.ClockHand;
  @Input() set percentage(value: number) {
    if(this.hand) {
      this._percentage = value;
      this.hand.showValue(value, 1000, am4core.ease.cubicOut);
    }
  }

  constructor() { }

  ngOnInit(): void {
    am4core.useTheme(am4themes_animated);
    // Themes end

    // create chart
    const chart = am4core.create("chartdiv", am4charts.GaugeChart);
    chart.innerRadius = -15;

    const axis = chart.xAxes.push((new am4charts.ValueAxis() as any));
    axis.min = 0;
    axis.max = 100;
    axis.strictMinMax = true;

    const colorSet = new am4core.ColorSet();

    const gradient = new am4core.LinearGradient();
    gradient.stops.push({color:am4core.color("green")})
    gradient.stops.push({color:am4core.color("yellow")})
    gradient.stops.push({color:am4core.color("red")})

    axis.renderer.line.stroke = gradient;
    axis.renderer.line.strokeWidth = 15;
    axis.renderer.line.strokeOpacity = 1;

    axis.renderer.grid.template.disabled = true;

    this.hand = chart.hands.push(new am4charts.ClockHand());
    this.hand.radius = am4core.percent(97);
  }
}
